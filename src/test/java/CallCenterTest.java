
import dispatcher.CallCenter;
import modelo.impl.Cliente;
import dispatcher.Dispatcher;
import modelo.impl.Director;
import modelo.impl.Operador;
import modelo.impl.Supervisor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class CallCenterTest {

    @Test
    public void diezLlamadas(){
        CallCenter callCenter = new CallCenter(10);

        List<Director> directores = new ArrayList<Director>();
        List<Supervisor> supervisores = new ArrayList<Supervisor>();
        List<Operador> operadores = new ArrayList<Operador>();

        directores.add(new Director("MOSALVE",callCenter.getCallCenterResources(), directores));
        directores.add(new Director("FELIPE",callCenter.getCallCenterResources(), directores));
        directores.add(new Director("SEBASTIAN CARDONA",callCenter.getCallCenterResources(), directores));
        directores.add(new Director("MARLON OLAYA",callCenter.getCallCenterResources(), directores));

        supervisores.add(new Supervisor("BELTRAN", callCenter.getCallCenterResources(), supervisores));
        supervisores.add(new Supervisor("MAO", callCenter.getCallCenterResources(), supervisores));


        operadores.add(new Operador("DANIEL", callCenter.getCallCenterResources(), operadores));
        operadores.add(new Operador("DEYBERTH", callCenter.getCallCenterResources(), operadores));
        operadores.add(new Operador("CARLOS", callCenter.getCallCenterResources(), operadores));
        operadores.add(new Operador("SANTA", callCenter.getCallCenterResources(), operadores));


        callCenter.setDirectores(directores);
        callCenter.setOperadores(operadores);
        callCenter.setSupervisores(supervisores);

        Dispatcher dispatcher = new Dispatcher(callCenter.getCallCenterResources(), operadores, supervisores, directores, callCenter.getLlamadas());

        dispatcher.InicializarDispatch();

        Assert.assertEquals(callCenter.getCallCenterResources().availablePermits(), 10);
        //PARA EL FALLO DE LA PRUEBA CAMBIAMOS LA CANTIDAD DE LLAMDAS ESPERADAS POR 9
       // Assert.assertEquals(callCenter.getCallCenterResources().availablePermits(), 10);

    }
}
