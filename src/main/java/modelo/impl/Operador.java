package modelo.impl;

import modelo.Empleado;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Operador extends Thread implements Empleado {
    public String nombre;
    private Semaphore callCenterResources;
    private List<Operador> empleados;

    public Operador(String nombre, Semaphore callCenterResources, List<Operador> operadores){
        this.nombre = nombre;
        this.callCenterResources = callCenterResources;
        this.empleados = operadores;
    }

    public void comenzarLlamada() {
        System.out.println("Operador " + this.nombre + " tomando llamada");
    }

    public void terminarLlamada() {
        System.out.println("Operador " + this.nombre + " finalizando llamda");
    }

    @Override
    public void run() {

        int tiempoLLamada = (int) (Math.random() * (10 - 5)) + 5;

        this.comenzarLlamada();

        try {
            sleep(tiempoLLamada * 1000);
            this.terminarLlamada();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Release Lock
            callCenterResources.release();
            this.empleados.add(new Operador(this.nombre, this.callCenterResources, this.empleados));
        }
    }
}
