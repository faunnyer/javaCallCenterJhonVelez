package modelo.impl;

import modelo.Empleado;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Director extends Thread implements Empleado {

    public String nombre;
    private Semaphore callCenterResources;
    private List<Director> empleados;

    public Director(String nombre, Semaphore callCenterResources, List<Director> directores){
        this.nombre = nombre;
        this.callCenterResources = callCenterResources;
        this.empleados = directores;
    }

    public void comenzarLlamada() {
        System.out.println("Director " + this.nombre + " tomando llamada");
    }

    public void terminarLlamada() {
        System.out.println("Director " + this.nombre + " finalizando llamada");
    }

    @Override
    public void run() {
        int callTime = (int) (Math.random() * (10 - 5)) + 5;

        this.comenzarLlamada();

        try {
            sleep(callTime * 1000);
            this.terminarLlamada();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Release Lock
            callCenterResources.release();
            this.empleados.add(new Director(this.nombre, this.callCenterResources, this.empleados));
        }
    }
}
