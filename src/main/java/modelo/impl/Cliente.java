package modelo.impl;

import java.util.LinkedList;

public class Cliente extends Thread {
    private Integer id;
    private LinkedList<Integer> llamadas;
    private Integer tiempoEspera;
    private String MENSAJE;

    public Cliente(Integer id, LinkedList<Integer> llamadas){
        this.id = id;
        this.llamadas=llamadas;
        tiempoEspera  = (int) ((Math.random() * (15 - 9)) + 9 ) * 1000;
        MENSAJE = "Cliente "+ this.id +" realizando llamada";
    }

    @Override
    public void run() {

        while(true){
            if(!llamadas.contains(this.id)){
                //hacemos una llamada
                System.out.println(MENSAJE);
                llamadas.add(this.id);
                try {
                    sleep(tiempoEspera);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

