package modelo.impl;

import modelo.Empleado;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Supervisor extends Thread implements Empleado {

    public String nombre;
    private Semaphore callCenterResources;
    private List<Supervisor> empleados;

    public Supervisor(String nombre, Semaphore callCenterResources, List<Supervisor> supervisores){
        this.nombre = nombre;
        this.callCenterResources = callCenterResources;
        this.empleados = supervisores;
    }

    public void comenzarLlamada() {
        System.out.println("Supervisor " + this.nombre + " tomando llamada");
    }

    public void terminarLlamada() {
        System.out.println("Supervisor " + this.nombre + " finalizando llamada");
    }

    @Override
    public void run() {

        int tiempoLLamada = (int) (Math.random() * (10 - 5)) + 5;

        this.comenzarLlamada();

        try {
            sleep(tiempoLLamada * 1000);
            this.terminarLlamada();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Release Lock
            callCenterResources.release();
            this.empleados.add(new Supervisor(this.nombre, this.callCenterResources, this.empleados));
        }
    }
}
