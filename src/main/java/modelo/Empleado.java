package modelo;

public interface Empleado {
    void comenzarLlamada();
    void terminarLlamada();
}
