package dispatcher;

import modelo.impl.Director;
import modelo.impl.Operador;
import modelo.impl.Supervisor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;


public class Dispatcher extends Thread {

    private static Semaphore SEMAFORO = new Semaphore(1);


    //INICIALIZAMOS EL LISTADO DE USUARIOS
    private List<Director> directores = new ArrayList<Director>();
    private List<Supervisor> supervisores = new ArrayList<Supervisor>();
    private List<Operador> operadores = new ArrayList<Operador>();
    private Semaphore callCenterResources;
    private LinkedList<Integer> llamadas =new LinkedList<Integer>();

    //RECIBIMOS LOS LISTADOS DE USUARIOS Y LLAMADAS
    public Dispatcher(Semaphore callCenterResources , List<Operador> operadores, List<Supervisor> supervisores, List<Director> directores, LinkedList<Integer> llamadas){
        this.callCenterResources=callCenterResources;
        this.llamadas = llamadas;
        this.directores = directores;
        this.operadores = operadores;
        this.supervisores = supervisores;
    }

    //INICIALIZAMOS DISPATCHER
    public void InicializarDispatch() {
        System.out.println("Inicializando dispatcher ...");
        this.start();
    }

    @Override
    public void run() {
        try {

            while (true) {

                // Bloque de llamdas bloqueadas
                SEMAFORO.acquire();

                // Realizamos si alguna llama esta esperando

                if(llamadas.size()>0){
                    // Asignamos una llamda
                    callCenterResources.acquire();

                    // Dispatch toma un empleado disponible
                    // primero operador, luego supervisor y por ultimo director
                    if(!operadores.isEmpty() || !supervisores.isEmpty() || !directores.isEmpty()){

                        Integer clientIde = llamadas.remove();

                        System.out.println("Recibiendo llamada del cliente  " + clientIde);

                        if (!operadores.isEmpty()) {

                            Operador operator = operadores.remove(0);
                            System.out.println("Asignando llamada del cliente: " + clientIde + " al operador: "+operator.nombre);
                            operator.start();

                        } else if (!supervisores.isEmpty()) {

                            Supervisor supervisor = supervisores.remove(0);
                            System.out.println("Asignando llamada del cliente: " + clientIde + " al supervisor : "+supervisor.nombre);
                            supervisor.start();

                        } else if(!directores.isEmpty()){

                            Director director = directores.remove(0);
                            System.out.println("Asignando llamada del cliente: " + clientIde + " al director: "+director.nombre);
                            director.start();
                        }

                    }else {

                        //se asigna acualqueir empleado apra liberar el recurso
                        callCenterResources.release();
                    }
                }

                SEMAFORO.release();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
