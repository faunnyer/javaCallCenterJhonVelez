package dispatcher;

import modelo.impl.Cliente;
import modelo.impl.Director;
import modelo.impl.Operador;
import modelo.impl.Supervisor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class CallCenter {
    private final int maximoLlamadas;
    private final Semaphore callCenterResources;

    //CREAMOS LISTA DE DIRECTORES
    private List<Director> directores = new ArrayList<Director>();

    //CREAMOS LISTA DE SUPERVISORES
    private List<Supervisor> supervisores = new ArrayList<Supervisor>();

    //CREAMOS LISTADO DE OPERADORES
    private List<Operador> operadores = new ArrayList<Operador>();

    //CREAMOS LISTADO DE LLAMADAS ENLAZADAS
    private LinkedList<Integer> llamadas = new LinkedList<Integer>();

    public CallCenter(int cantidadLlamdas){
        this.maximoLlamadas = cantidadLlamdas;
        this.callCenterResources = new Semaphore(maximoLlamadas, true);
    }

    private void iniciarCallCenter() {

        //LLAMAMOS LOS METODOS QUE NOS PERMETIRA CREAR LOS USUARIOS
        crearDirectores();
        crearSupervisores();
        crearOperadores();


        //INSTANCIAMOS DISPATCHER Y ENVIAMOS COMO ARGUMENTO LOS LSITADOS  DE USUARIOS CREADOS
        Dispatcher dispatcher = new Dispatcher(callCenterResources, operadores, supervisores, directores, llamadas);
        // LUEGO INICIALIZAMOS EL DISPATCHER
        dispatcher.InicializarDispatch();

        // REALZIAMOS UN CICLO DE 1 A 10 PARA SIMULAR LOS CLIENTES
        for (int i = 0; i < 10; i++) {
            Cliente cliente = new Cliente(i, llamadas);

            //LLAMAMOS START DEL thread PARA ENTRAR A LA CLASE
            cliente.start();
        }

    }

    public void crearDirectores(){
        directores.add(new Director("Marlon Olaya",callCenterResources, directores));
        directores.add(new Director("Jhon Velez",callCenterResources, directores));
        directores.add(new Director("Faunnyer Hernandez",callCenterResources, directores));
        directores.add(new Director("Manuela Bustamante",callCenterResources, directores));
    }

    public void crearSupervisores(){
        supervisores.add(new Supervisor("Kiara Velez", callCenterResources, supervisores));
        supervisores.add(new Supervisor("Mariana Castaño", callCenterResources, supervisores));
    }

    public void crearOperadores(){
        operadores.add(new Operador("Daniel", callCenterResources, operadores));
        operadores.add(new Operador("Laura", callCenterResources, operadores));
        operadores.add(new Operador("Cristian", callCenterResources, operadores));
    }

    public List<Director> getDirectores() {
        return directores;
    }

    public List<Supervisor> getSupervisores() {
        return supervisores;
    }

    public List<Operador> getOperadores() {
        return operadores;
    }

    public void setDirectores(List<Director> directores) {
        this.directores = directores;
    }

    public void setSupervisores(List<Supervisor> supervisores) {
        this.supervisores = supervisores;
    }

    public void setOperadores(List<Operador> operadores) {
        this.operadores = operadores;
    }

    public Semaphore getCallCenterResources() {
        return callCenterResources;
    }

    public LinkedList<Integer> getLlamadas() {
        return llamadas;
    }


    public static void main(String[] args) {
        CallCenter callCenter = new CallCenter(10);
        callCenter.iniciarCallCenter();
    }
}
